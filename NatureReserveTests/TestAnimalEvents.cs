using NatureReserveSolution;

namespace NatureReserveTests;

class TestAnimalEvents : IAnimalEvents
{
    public bool IsStarving = false;
    public int IsStarvingCount = 0;
    public void Starving(string name, int currentEnergy, string foodName)
    {
        IsStarving = true;
        IsStarvingCount++;
    }
}
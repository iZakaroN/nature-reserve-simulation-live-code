using NatureReserveSolution;

namespace NatureReserveTests;

class TestAnimal : Animal
{
    public TestAnimal(
        int maxEnergy,
        int nutrition,
        HashSet<Food> diet,
        IAnimalEvents eventReceiver)
        : base(maxEnergy, nutrition, eventReceiver)
    {
        Diet = diet;
    }

    protected override HashSet<Food> Diet { get; }
}
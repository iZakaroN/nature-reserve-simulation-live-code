using NatureReserveSolution;

namespace NatureReserveTests;

class TestEnergyViolationAnimal : Animal
{
    private readonly Food _eatableFood;

    public TestEnergyViolationAnimal(Food eatableFood)
        : base(10, 19, new NullAnimalEvents())
    {
        _eatableFood = eatableFood;
    }

    protected override HashSet<Food> Diet => new HashSet<Food>() { _eatableFood };

    protected override int Consume(Food food)
    {
        _currentEnergy = 199999;
        return 0;
    }
}
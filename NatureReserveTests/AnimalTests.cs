using Moq;
using NatureReserveSolution;

namespace NatureReserveTests
{
    [TestClass]
    public class AnimalTests
    {
        [TestMethod]
        public void Feed_PassNotEatableFood_MustBeStarving()
        {
            //assign
            var testEvents = new TestAnimalEvents();
            var animal = new TestAnimal(
                10,
                10,
                new HashSet<Food>() { },
                testEvents);

            //Act
            animal.Feed(Foods.Berry);
            animal.Feed(Foods.Berry);
            animal.Feed(Foods.Berry);

            //Assert
            Assert.IsTrue(testEvents.IsStarving);
            Assert.AreEqual(3, testEvents.IsStarvingCount);
        }

        [TestMethod]
        public void Feed_PassEatableFood_MustNotBeStarving_()
        {
            //assign
            var testEvents = new TestAnimalEvents();
            var animal = new TestAnimal(
                10,
                10,
                new HashSet<Food>() { Foods.Berry },
                testEvents);

            //Act
            animal.Feed(Foods.Berry);

            //Assert
            Assert.AreEqual(1, testEvents.IsStarvingCount);
        }

        [TestMethod]
        public void Feed_PassEatableFood_MustNotBeStarving()
        {

            //assign
            //var testEvents = new TestAnimalEvents();
            var testEventsMoq = new Mock<IAnimalEvents>();
            var animal = new TestAnimal(
                10,
                10,
                new HashSet<Food>() { Foods.Berry },
                testEventsMoq.Object);

            //Act
            animal.Feed(Foods.Berry);
            animal.Feed(Foods.Berry);

            //Assert
            testEventsMoq.Verify(
                (animalEvent) => animalEvent.Starving(
                    animal.Name, 9,Foods.Berry.Name));
            //Assert.AreEqual(1, testEvents.IsStarvingCount);
        }


        [TestMethod]
        public void Feed_ExceedEnergy_ThrowInvalidDataException()
        {
            //assign
            var animal = new TestEnergyViolationAnimal(Foods.Berry);
            //Act
            Assert.ThrowsException<InvalidDataException>(
                () => animal.Feed(Foods.Berry));
        }

    }
}
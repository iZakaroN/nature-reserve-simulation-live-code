﻿namespace NatureReserveSolution;

public class Food
{
    public readonly string Name;
    public int _nutrition;

    public Food(int nutrition)
    {
        _nutrition = nutrition;
        Name = GetType().Name;
    }

    public virtual int ConsumedNutrition(int missingEnergy)
    {
        int consumedNutrition = missingEnergy;
        if (_nutrition < missingEnergy)
        {
            consumedNutrition = _nutrition;
        }

        _nutrition -= consumedNutrition;
        return consumedNutrition;
    }

    public override bool Equals(object? obj) 
        => obj is Food food && Name.Equals(food.Name);

    public override int GetHashCode()
        => Name.GetHashCode();
    public override string ToString()
        => Name;
}
﻿namespace NatureReserveSolution
{
    public static class Foods
    {
        public static readonly Food Berry = new Berry();
        public static readonly Food Grass = new Grass();
        public static readonly Food Rabbit = new Rabbit();
        //public static readonly Food Goat = new Goat();
    }
}

﻿namespace NatureReserveSolution;

class Goat : Animal
{
    private static readonly HashSet<Food> MyDiet = new()
    {
        Foods.Grass,
    };

    public Goat(IAnimalEvents eventsReceiver) 
        : base(7, 7, eventsReceiver)
    {
    }

    protected override HashSet<Food> Diet => MyDiet;
}
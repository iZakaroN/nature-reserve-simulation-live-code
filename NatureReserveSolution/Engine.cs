﻿using System.Xml.Linq;

namespace NatureReserveSolution
{
    public class ConsoleLogger : IAnimalEvents
    {
        public bool Enabled
        {
            get;
            set;
        }

        public void WriteLine(string text)
        {
            if (Enabled)
                Console.WriteLine(text);
        }

        public void Starving(string name, int currentEnergy, string foodName)
        {
            WriteLine($"{name} does not eat {foodName}");
        }
    }
    internal class Engine
    {
        ConsoleLogger _looger = new ConsoleLogger();
        public void Run()
        {
            var food = new[]
            {
                Foods.Berry,
                //Foods.Goat,
                Foods.Rabbit,
                Foods.Grass
            };

            var animals = new Animal[]
            {
                new Wolf(_looger),
                //new Goat(),
                //new Wolf(),
                new Rabbit(),
                new Rabbit(),
                //new Wolf(),
            };
            var detailedLog = true;
            var random = new Random();
            bool aliveAnimals;
            do
            {
                aliveAnimals = false;
                foreach (var animal in animals)
                {
                    animal.Feed(food[random.Next(food.Length)]);
                    animal.IncreaseLifespan();
                    aliveAnimals = aliveAnimals || animal.IsAlive;

                }

                var key = Console.ReadKey();
                if (key.Key == ConsoleKey.A)
                    detailedLog = !detailedLog;
                if (!detailedLog)
                    TurnSummary(animals);

            } while (aliveAnimals);

        }

        private void TurnSummary(Animal[] animals)
        {
            
        }
    }
}

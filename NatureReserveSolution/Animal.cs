﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureReserveSolution
{
    public interface IAnimalEvents
    {
        void Starving(string name, int currentEnergy, string foodName);
    }
    public abstract class Animal : Food
    {
        private readonly IAnimalEvents _eventReceiver;
        private readonly int _maxEnergy;
        protected int _currentEnergy;
        public int Lifespan { get; private set; }

        protected Animal(int maxEnergy, int nutrition, IAnimalEvents eventReceiver)
            : base(nutrition)
        {
            _eventReceiver = eventReceiver;
            _currentEnergy = _maxEnergy = maxEnergy;
        }

        protected abstract HashSet<Food> Diet { get; }
        public void Feed(Food food)
        {
            if (IsAlive)
            {
                if (Diet.Contains(food) && Consume(food) > 0)
                {
                    Eating(food);
                }
                else
                {
                    Starving(food);
                }
            }

            if (_currentEnergy > _maxEnergy)
                throw new InvalidDataException("Current energy exceed maximum energy");
        }

        protected virtual int Consume(Food food)
        {
            if (_currentEnergy < _maxEnergy)
            {
                var consumed = food.ConsumedNutrition(_maxEnergy - _currentEnergy);
                _currentEnergy += consumed;
                return consumed;
            }
            return 0;
        }

        protected virtual void Starving(Food food)
        {
            _currentEnergy--;
            _eventReceiver.Starving(Name, _currentEnergy, food.Name);

        }

        protected virtual void Eating(Food food)
        {
            Console.WriteLine($"{Name} eat {food}");
        }

        public void IncreaseLifespan()
        {
            Lifespan++;
        }

        public bool IsAlive => _currentEnergy > 0;
    }
}

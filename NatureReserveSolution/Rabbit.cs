﻿namespace NatureReserveSolution;

public record Statistics(int MinLifeSpan, int MaxLifeSPan);

public class Statistic1
{
    public int MinLifeSpan { get; }
    public int MaxLifeSPan { get; }

    public Statistic1(int minLifeSpan, int maxLifeSPan)
    {
        MinLifeSpan = minLifeSpan;
        MaxLifeSPan = maxLifeSPan;
    }
}

class Stat2 : Statistic1
{
    public Stat2(int minLifeSpan, int maxLifeSPan) : base(minLifeSpan, maxLifeSPan)
    {
    }
}
public class Rabbit : Animal
{
    private static readonly HashSet<Food> YoungDiet = new()
    {
        Foods.Grass
    };

    private static readonly HashSet<Food> MatureDiet = new()
    {
        Foods.Berry,
    };

    public Rabbit()
        : base(5, 5, null)//TODO: Pass event receiver
    {
    }

    protected override HashSet<Food> Diet => 
        Lifespan < 10 
            ? YoungDiet
            : YoungDiet.Concat(MatureDiet).ToHashSet();

    protected override void Eating(Food food)
    {
        base.Eating(food);
        Console.WriteLine("Bau");
    }
}
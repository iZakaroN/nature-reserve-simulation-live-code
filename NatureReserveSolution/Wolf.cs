﻿namespace NatureReserveSolution;

public class Wolf : Animal
{
    private static readonly HashSet<Food> MyDiet = new()
    {
        Foods.Rabbit,
        //Foods.Goat,
    };

    public Wolf(IAnimalEvents eventsReceiver)
        : base(10, 10, eventsReceiver)
    {
    }

    protected override HashSet<Food> Diet => MyDiet;
}